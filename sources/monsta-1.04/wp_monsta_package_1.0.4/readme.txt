07/10/2019: Updated version 1.0.4
	- Fixed product stock quantity and SKU in product page 

05/10/2019: Updated version 1.0.3
	- Updated Woocommerce 3.6.2
	- Fixed error orderby shop page in the mobile.

03/29/2019: Updated version 1.0.2
	- Updated WooCommerce 3.5.5

01/08/2019: Updated version 1.0.1
	- Updated WooCommerce 3.5.3
	
11/24/2018: Updated black version